<?php
// Define the webserver and path parameters
// * DIR_FS_* = Filesystem directories (local/physical)
// * DIR_WS_* = Webserver directories (virtual/URL)

// File System
Define ("DIR_FS_WEBROOT", $_SERVER['DOCUMENT_ROOT']);
Define ("DIR_FS_SITEROOT", DIR_FS_WEBROOT . dirname(dirname($_SERVER['PHP_SELF'])) . '/' );
Define ("DIR_FS_SITEBAK", DIR_FS_SITEROOT . "bak/");
Define ("DIR_FS_XML", DIR_FS_SITEROOT . "xml/");
Define ("DIR_FS_INCLUDES" , DIR_FS_SITEROOT . "includes/");
Define ("DIR_FS_FUNCTIONS", DIR_FS_INCLUDES . "functions/");


// Web System
Define ("DIR_WS_HTTP_ROOT", "http://localhost/learnhebrew/");
Define ("DIR_WS_INCLUDES" , DIR_WS_HTTP_ROOT . "includes/");
Define ("DIR_WS_XML" , "xml/");
Define ("DIR_WS_CLASS"    , DIR_WS_INCLUDES . "class/");
Define ("DIR_WS_FUNCTIONS", DIR_WS_INCLUDES . "functions/");
Define ("DIR_WS_BOXES", DIR_WS_INCLUDES . "boxes/");
?>
