<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<?php
require ("includes/configure.php");
$xmlFileName =  "mispar.xml";
$Path     = "/var/www/html/hebrewa/";
$xmlFullPath    = DIR_FS_XML . "mispar.xml";
require (DIR_FS_FUNCTIONS . "xml.php");
require (DIR_FS_FUNCTIONS . "functions.php");
session_name('Private');
session_start();

function F_Soort(){
    $xml = $_SESSION['xml'];
    $tempXml =  SoortXmlMisparim(simplexml_load_string($xml));
    $_SESSION['xml'] = $tempXml;
    WOS();
}

function WOS() {
    $xml = simplexml_load_string($_SESSION["xml"]);
    echo "<form method=\"post\" action=\"\" name=\"ext\">\n";
    echo "<input type=\"hidden\" name=\"ext\" value=\"exty\">\n";
    echo "<input type=\"submit\" value=\"exit\">\n</form>";

    echo "<table border=\"1\" cellpadding=\"0\">\n";
    foreach ($xml as $key=>$valueP) {
        echo "\t<tr>\n";
        echo "\t<form method=\"GET\" action=\"\" name=\"Form1\">\n";
        foreach ($valueP as $key=>$value) {
            switch ($value->getname()) {
                case Number:
                  $Lenght= 1;
                        Break;
                case il:
                  $Lenght= 10;
                        Break;
                case ilm:
                  $Lenght= 10;
                        Break;
                case ilf:
                  $Lenght= 10;
                        Break;
                case ilPh:
                  $Lenght= 10;
                        Break;
                case ilPhM:
                  $Lenght= 10;
                        Break;
                case ilPhF:
                  $Lenght= 10;
                        Break;
            }
            echo "\t\t<td><input type=\"text\" name=\"". $value->getname() . "\" value=\"" . $value . "\" size=\"" . $Lenght . "\"></td>\n";
        }
        echo "\t\t<td><input type=\"submit\" name=\"upd\" value=\"Submit\"></td>\n";
        echo "\t</form>\n";
        echo "\t</tr>\n";
    }
    echo "<tr><form>\n";
    echo "<td><input type=\"text\" name=\"Number\" value=\"\" size=\"1\"></td>\n";
    echo "<td><input type=\"text\" name=\"il\" value=\"\" size=\"10\"></td>\n";
    echo "<td><input type=\"text\" name=\"ilm\" value=\"\" size=\"10\"></td>\n";
    echo "<td><input type=\"text\" name=\"ilf\" value=\"\" size=\"10\"></td>\n";
    echo "<td><input type=\"text\" name=\"ilPh\" value=\"\" size=\"10\"></td>\n";
    echo "<td><input type=\"text\" name=\"ilPhM\" value=\"\" size=\"10\"></td>\n";
    echo "<td><input type=\"text\" name=\"ilPhF\" value=\"\" size=\"10\"></td>\n";
    echo "<td><input type=\"submit\" name=\"add\" value=\"Add\"></td>\n";
    echo "<input type=\"hidden\" name=\"upd\" value=\"less\">\n";
    echo "<form>\n</tr>\n";
} // eof WOS

function F_Update($ReadRequest) {
    $xml    = simplexml_load_string($_SESSION["xml"]);
    //workaround xpath is not posible @ this moment on the local server
    $cCount = 0;
    $line   = null;
    $found  = false;
    $Number = intval($ReadRequest['Number']);
    $max    = count($xml);
    while ($found==FALSE && $cCount<=$max) {
        if (intval($xml->{mispar}[$cCount]->attributes() )== $Number) {
            $line  = $cCount;
            $found = TRUE;
        }
          $cCount++;
    }// eof workaround

    $xml->{mispar}[$line]->{il}    = $ReadRequest['il'];
    $xml->{mispar}[$line]->{ilm}   = $ReadRequest['ilm'];
    $xml->{mispar}[$line]->{ilf}   = $ReadRequest['ilf'];
    $xml->{mispar}[$line]->{ilPh}  = $ReadRequest['ilPh'];
    $xml->{mispar}[$line]->{ilPhM} = $ReadRequest['ilPhM'];
    $xml->{mispar}[$line]->{ilPhF} = $ReadRequest['ilPhF'];
    $_SESSION["xml"] = RecursiveXML($xml);
    WOS();
} // eof F_Update

function F_Add($ReadRequest) {
    $xml = simplexml_load_string($_SESSION["xml"]);
    $Add_Parrent = $xml->addChild('mispar');
    $Add_Parrent->addAttribute('id', $ReadRequest['Number']);
    $Add_Child = $Add_Parrent->addChild('Number',  $ReadRequest['Number']);
    $Add_Child = $Add_Parrent->addChild('il',  $ReadRequest['il']);
    $Add_Child = $Add_Parrent->addChild('ilm',   $ReadRequest['ilm']);
    $Add_Child = $Add_Parrent->addChild('ilf',   $ReadRequest['ilf']);
    $Add_Child = $Add_Parrent->addChild('ilPh',  $ReadRequest['ilPh']);
    $Add_Child = $Add_Parrent->addChild('ilPhM', $ReadRequest['ilPhM']);
    $Add_Child = $Add_Parrent->addChild('ilPhF', $ReadRequest['ilPhF']);
    $_SESSION["xml"] = RecursiveXML($xml);
    WOS();
} // eof F_Add

function ReadStr() {
    $ReadRequest = array();
    $ReadRequest['Number'] = intval($_REQUEST['Number']);
    $ReadRequest['il']     = $_REQUEST['il'];
    $ReadRequest['ilm']    = $_REQUEST['ilm'];
    $ReadRequest['ilf']    = $_REQUEST['ilf'];
    $ReadRequest['ilPh']   = $_REQUEST['ilPh'];
    $ReadRequest['ilPhM']  = $_REQUEST['ilPhM'];
    $ReadRequest['ilPhF']  = $_REQUEST['ilPhF'];
    Return $ReadRequest;
}

function UnsetStr() {
/*      unset($ReadRequest['Number'];
      unset($ReadRequest['il'];
      unset($ReadRequest['ilm'];
      unset($ReadRequest['ilf'];
      unset($ReadRequest['ilPh'];
      unset($ReadRequest['ilPhM'];
      unset($ReadRequest['ilPhF'];*/
}

function F_SaveFile($file, $fullSourcePath) {
    $xml = $_SESSION['xml'];
    $newfile = "";
    $newfile = DIR_FS_SITEBAK . "Misparim/" . $file. "." . date("Ymd_Hi_s") .".bak";
    if (copy($fullSourcePath, $newfile) ) {
        $file_handle = fopen($newfile,"w") or die("Bestand niet schrijfbaar");
        $content .="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n";
        $content .="<!DOCTYPE misparim SYSTEM  \"misparim.dtd\">";
        $content .= $xml;
        fwrite($file_handle, $content);
        fclose($file_handle);
    }else{
        echo "failed to copy $newfile...\n<br>";
    }
    unset($_REQUEST["ext"]);
    unset($_SESSION["xml"]);
    unset($newfile);
}

function F_LoadFile($useFile) {
    unset($handel);
    if (file_exists($useFile) ) {
        $xml = simplexml_load_file($useFile);
        $_SESSION['xml'] = RecursiveXML($xml);
        unset($userFile);
        unset($handel);
        unset($xml);
        WOS();
    }else{
        echo "bestand bestaat niet";
    }
}


echo "<html>\n<head>\n\t<title></title>\n\t<meta http-equiv=Content-Type content=\"text/html; charset=UTF-8\">\n";
echo "\t<link rel=\"stylesheet\" type=\"text/css\" href=\"stylesheet.css\">\n</head>\n<body>\n";
echo "Begin";

if (isset($_REQUEST["ext"]) == false) {
    if (empty($_SESSION['xml']) ) F_LoadFile($xmlFullPath);
} else {
    switch ($_REQUEST) {
        case (isset($_REQUEST["ext"] ) ):
    //        unset($_SESSION["xml"]);
            F_SaveFile($xmlFileName, $xmlFullPath);
            echo "EXIT";
            Break;
        case (isset($_REQUEST['upd']) && $_REQUEST['upd']==="Submit"):
            $ReadRequest = ReadStr();
            F_Update($ReadRequest);
            Break;
        case (isset($_REQUEST['add']) && $_REQUEST['add']==="Add"):
            $ReadRequest = ReadStr();
            F_Add($ReadRequest);
              Break;
        case (isset($_REQUEST['srt']) && $_REQUEST['srt']==="Srt"):
            F_Soort();
            Break;
        default :
            if (empty($_SESSION['xml']) ) F_LoadFile($xmlFullPath);
            break;
    }
}
echo "end";
echo "</table>\n</body>\n</html>\n";
?>
