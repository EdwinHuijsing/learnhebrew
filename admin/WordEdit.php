<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<?php
require_once ("includes/configure.php");
$ReadFile = "millim.xml";
$Path     = "/var/www/html/hebrewa/";
$File     = DIR_FS_XML . $ReadFile;
$docType  = "<!DOCTYPE words SYSTEM  \"words.dtd\">\n";
require_once (DIR_FS_FUNCTIONS . "xml.php");
require_once (DIR_FS_FUNCTIONS . "functions.php");
session_name('Private');
session_start();

function WOS($handel)
{
    if (!is_object($handel) ) {
    $xml = simplexml_load_string($handel);
    } else {
        $xml = $handel;
    }

    $count=0;
    foreach ($xml as $key=>$value)
    {
        foreach ($value as $key1=>$value1)
        {
            if (($key1 == 'name') == FALSE) $count++;
        }
    }

    $count++;
    echo "<table>\n";
    echo "\t<form>\n";
    echo "\t\t<tr><td align=right>categorie :</td>";
    echo "\t\t<td><select name='categorie'>\n";
    foreach ($xml->categorie as $key=>$value)
    {
        echo "\t\t\t<option>" . $value->name . "</option>\n";
    }

    unset($tXml);
    echo "\t\t</select></td></tr>\n";
    echo "\t\t<tr><td align=right>en :</td>     <td><input type=\"text\" name=\"en\">     </td></tr>\n";
    echo "\t\t<tr><td align=right>ph :</td>     <td><input type=\"text\" name=\"ph\">     </td></tr>\n";
    echo "\t\t<tr><td align=right>phm :</td>    <td><input type=\"text\" name=\"phm\">    </td></tr>\n";
    echo "\t\t<tr><td align=right>phf :</td>    <td><input type=\"text\" name=\"phf\">    </td></tr>\n";
    echo "\t\t<tr><td align=right>il :</td>     <td><input type=\"text\" name=\"il\">     </td></tr>\n";
    echo "\t\t<tr><td align=right>ilm :</td>    <td><input type=\"text\" name=\"ilm\">    </td></tr>\n";
    echo "\t\t<tr><td align=right>ilf :</td>    <td><input type=\"text\" name=\"ilf\">    </td></tr>\n";
    echo "\t\t<tr><td align=right><input type=\"submit\" name=\"add\" value=\"Add\"></td>\n";
    echo "\t\t<tr><input type=\"hidden\" name=\"upd\" value=\"less\"></tr>\n";
    echo "\t\t<tr><input type=\"hidden\" name=\"Number\" value=\"" . $count . "\"></tr>\n";
    echo "\t</form>\n";
    echo "</table>\n";
} // eof WOS

function fop_Update($arr, $root)
{
    $xml    = simplexml_load_string($_SESSION["xml"]);
}

/**
 *
 *    @param array $ReadRequest
 */
function F_Update($ReadRequest) {
    //workaround xpath is not posible @ this moment on the local server
//     $cCount = 0;
//     $line   = null;
//     $found  = false;
//     $Number = intval($ReadRequest['Number']);
//     $max    = count($xml);
//     while ($found==FALSE && $cCount<=$max) {
//         if (intval($xml->{mispar}[$cCount]->attributes() )== $Number) {
//             $line  = $cCount;
//             $found = TRUE;
//         }
//           $cCount++;
//     }// eof workaround
//
//     $xml->{mispar}[$line]->{il}    = $ReadRequest['il'];
//     $xml->{mispar}[$line]->{ilm}   = $ReadRequest['ilm'];
//     $xml->{mispar}[$line]->{ilf}   = $ReadRequest['ilf'];
//     $xml->{mispar}[$line]->{ilPh}  = $ReadRequest['ilPh'];
//     $xml->{mispar}[$line]->{ilPhM} = $ReadRequest['ilPhM'];
//     $xml->{mispar}[$line]->{ilPhF} = $ReadRequest['ilPhF'];
//     $_SESSION["xml"] = RecursiveXML($xml);
//     WOS();
} // eof F_Update

function F_Add($ReadRequest) {
    $xml = simplexml_load_string($_SESSION["xml"]);
    $Add_Parrent = $xml->addChild('mispar');
    $Add_Parrent->addAttribute('id', $ReadRequest['Number']);
    $Add_Child = $Add_Parrent->addChild('Number',  $ReadRequest['Number']);
    $Add_Child = $Add_Parrent->addChild('il',  $ReadRequest['il']);
    $Add_Child = $Add_Parrent->addChild('ilm',   $ReadRequest['ilm']);
    $Add_Child = $Add_Parrent->addChild('ilf',   $ReadRequest['ilf']);
    $Add_Child = $Add_Parrent->addChild('ilPh',  $ReadRequest['ilPh']);
    $Add_Child = $Add_Parrent->addChild('ilPhM', $ReadRequest['ilPhM']);
    $Add_Child = $Add_Parrent->addChild('ilPhF', $ReadRequest['ilPhF']);
    $_SESSION["xml"] = RecursiveXML($xml);
    WOS();
} // eof F_Add

function fop_Add($readRequest, $handel) {
    if (!is_object($handel) )
      {
        $xml = simplexml_load_string($handel);
      }
    else
      {
        $xml = $handel;
    }
    $cat = $readRequest["categorie"];
    $parrent = $xml->xpath('//categorie[@name="' . $cat . '"]');
    $Add_Parrent = $parrent[0]->addChild('word');
    $Add_Parrent->addAttribute('id', $readRequest['Number']);

    foreach ($readRequest as $key=>$value)
    {
      // Not all that is passed thrue i want to see, so they are removed
      switch($key)
      {
        case "categorie":
                break;
        case "add":
                break;
        case "upd":
                break;
        case "Private":
                break;
        case "Number":
                break;
        default:
                $Add_Child = $Add_Parrent->addChild($key, $value);
      }
    }
    $_SESSION["xml"] = RecursiveXML($xml);
} // eof fop_Add

function ReadStr() {
    $ReadRequest = array();
    $ReadRequest['Number'] = intval($_REQUEST['Number']);
    $ReadRequest['il']     = $_REQUEST['il'];
    $ReadRequest['ilm']    = $_REQUEST['ilm'];
    $ReadRequest['ilf']    = $_REQUEST['ilf'];
    $ReadRequest['ilPh']   = $_REQUEST['ilPh'];
    $ReadRequest['ilPhM']  = $_REQUEST['ilPhM'];
    $ReadRequest['ilPhF']  = $_REQUEST['ilPhF'];
    Return $ReadRequest;
}

function UnsetStr() {
$_SESSION['xml'];/*      unset($ReadRequest['Number'];
      unset($ReadRequest['il'];
      unset($ReadRequest['ilm'];
      unset($ReadRequest['ilf'];
      unset($ReadRequest['ilPh'];
      unset($ReadRequest['ilPhM'];
      unset($ReadRequest['ilPhF'];*/
}

function F_LoadFile($useFile) {
    if (file_exists($useFile) ) {
        $xml = simplexml_load_file($useFile);
        $_SESSION['xml'] = $xml;
        unset($userFile);
        unset($xml);
    }else{
        echo "bestand bestaat niet";
    }
}

echo "<html>\n<head>\n\t<title></title>\n\t<meta http-equiv=Content-Type content=\"text/html; charset=UTF-8\">\n";
echo "\t<link rel=\"stylesheet\" type=\"text/css\" href=\"stylesheet.css\">\n</head>\n<body>\n";
echo "Begin";
if (empty($_SESSION['xml']) )
  { echo $File;
    F_LoadFile($File);
}

if (isset($_REQUEST["ext"]) == false) {
    unset($_SESSION['xml']);
    F_LoadFile($File);
    echo RecursiveXML($_SESSION['xml'], "xmltable");
    echo "<br>\n";
    WOS($_SESSION['xml']);
} else {
    switch ($_REQUEST) {
        case (isset($_REQUEST["ext"] ) ):
            $t = RecursiveXML($_SESSION["xml"]);
            fop_SaveFile($t, $ReadFile, "words", $docType);
            echo RecursiveXML($_SESSION['xml'], "xmltable");
            echo "<br>\n";
            WOS($_SESSION['xml']);
            Break;

        case (isset($_REQUEST['upd']) && $_REQUEST['upd']==="Submit"):
            $ReadRequest = ReadStr();
            F_Update($ReadRequest);
            Break;

        case (isset($_REQUEST['add']) && $_REQUEST['add']==="Add"):
              $ReadRequest = checkUrlGetWord();
              fop_Add($ReadRequest, $_SESSION['xml']);
              echo RecursiveXML($_SESSION['xml'], "xmltableEdit");
              WOS;
              Break;

        default :
            unset($_SESSION['xml']);
            F_LoadFile($File);
            echo RecursiveXML($_SESSION['xml'], "xmltable");
            echo "<br>\n";
            WOS($_SESSION['xml']);
            break;
    }
}
echo "</table>";
echo "end\n";
echo "</body>\n</html>\n";
?>
