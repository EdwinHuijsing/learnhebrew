<?php
function GetXmlHeader($file) {
    $xmlHead = "";
    $eofDoc == FALSE;
    $handel = fopen($file, r);
    while ($eofDoc == FALSE) {
    $xmlHead .= $searchStr = fgets($handel);
        if(stristr($searchStr, "<!DOCTYPE") && stristr($searchStr, ">") ) {
                $eofDoc = TRUE;
        }
        if(stristr($searchStr, "<!DOCTYPE") && StriStr ($searchStr, "[") ) {
            while (StriStr ($searchStr, "]>")==FALSE) {
                $xmlHead .= $searchStr = fgets($handel);
            }
            $eofDoc = TRUE;
        }
    }
    fclose($handel);
    unset($searchStr);
    Return $xmlHead;
}


/* * ********************************************************************** * */
function wrapAttributes($handel) {
  // find all attributes from this child

  // to avoid error we will always return a empty string
  $return = "";

  // only if there are some attributes we want ro return them
  if (count($handel->attributes() ) > 0 ) {
    // how many attributes are there?
    foreach ($handel->attributes() as $key=>$value ) {
        // prepare the return string
        $return .= " " . $key . "=\"" . $value . "\"";
      }
    }
  return $return;
}

function RecursiveXMLFile($handel, $preffix = "") {
    // to make the file readable you need a priffix defin that here
    $prePrefFix = "  ";

    // if we have reacht the last child we should not try to go deeper
    if (count($handel->children() ) < 1 ) {
        // open the tag and check attributes
        $ret0 = $preffix . "<" . $handel->getname() . wrapAttributes($handel) . ">";
        // insert the value between the tags
        $ret0 .= $handel;
        // close the tag
        $ret0 .= "</" . $handel->getname() . ">\n";
        // we are ready to back a stap
        return $ret0;
      }

    // prepare the return string; open the tag we are now in and check attributes
    $ret =  $preffix . "<" . $handel->getname() . wrapAttributes($handel) . ">\n";

    // go to the next level
    foreach ($handel->children() as $key => $child) {
        $ret .= RecursiveXMLFile($child, $preffix . $prePrefFix);
      }
    // the only thing we now need is the closing tag
    $ret .=  $preffix . "</" . $handel->getname() . ">\n";
    // we are ready to back a stap
    Return $ret;
  }

function RecursiveXMLText($handel, $preffix = "") {
      // to make the file readable you need a priffix defin that here
      $prePrefFix = "|---";

      // if we have reacht the last child we should not try to go deeper
      if (count($handel->children() ) < 1 ) {
          // open the tag and check attributes
          $ret0 = $preffix . "&lt;" . $handel->getname() . wrapAttributes($handel) . "&gt;";
          // insert the value between the tags
          $ret0 .= $handel;
          // close the tag
          $ret0 .= "&lt;/" . $handel->getname() . "&gt;<br>\n";
          // we are ready to back a stap
          return $ret0;
        }

      // prepare the return string; open the tag we are now in and check attributes
      $return =  $preffix . "&lt;" . $handel->getname() . wrapAttributes($handel) . "&gt;<br>\n";

      // go to the next level
      foreach ($handel->children() as $key => $child) {
          $return .= RecursiveXMLText($child, $preffix .  $prePrefFix);
        }
      // the only thing we now need is the closing tag
      $return .=  $preffix . "&lt;/" . $handel->getname() . "&gt;<br>\n";
      // we are ready to back a stap
      Return $return;
}

/**
 *
 *
 * When call this function from out site this function $opt need to be TRUE
 *
 *    @param simpleXMLobject  $handel
 *    @param string           $preffix
 *    @param boolean          $opt
 */
function RecursiveXMLTable($handel, $preffix = "", $opt = TRUE) {
      // we have to build the table correct
      if ($opt === TRUE)
        {
          $return .= "<table border=\"1\">\n";
      }

      // to make the file readable you need a priffix define that here
      $prePrefFix = "\t";

      // if we have reacht the last child we should not try to go deeper
      if (count($handel->children() ) < 1 ) {
          if ( empty($handel) ) $handel = "&nbsp;";
          //add cel with child info
          $ret0 .=  $preffix . "<td>" . $handel . "</td>\n";
          return $ret0;
        }

      // Only wen there are no more childeren with the currend child we need to start
      // building a new row
      if (count($handel->children()->children() ) < 1 ) {
            $return .= $preffix . "<tr>\n";
          }

      // go to the next level
      foreach ($handel->children() as $key => $child) {
          $return .= RecursiveXMLTable($child, $preffix .  $prePrefFix, FALSE);
        }
      if (count($handel->children()->children() ) < 1 ) {
            $return .= $preffix . "</tr>\n";
          }
      if ($opt === TRUE){
          $return .= "</table>";
        }
      Return $return;
  }

/**
 *  Create a html table of the xml, with input fields
 *
 *  If we need to be able to edit the content of the xml we can use this function
 *  It shows the content in a html table. All info in put in rows as a form
 *  so that it can be saved after the changes has been made.
 *
 *    @param simpleXMLobject  $handel     This can also be the xmlobject that is in a tag.
 *    @param string           $preffix    To make the sourcecode readable we need to add the prefix like \t
 *    @param boolean          $rootLevel  Optional, if we start the recurive we
 *                                        are not in thus need to be FALSE.
 *    @param string           $parent     Optional, While creating the table we
 *                                        need to know the name of the parent tag
 *                                        we are in. This a html table field string.
 *    @param string           $childName  Optional, while create the table we
 *                                        need to name of the tag we are printing.
 */
function RecursiveXMLTableEdit($handel, $preffix = "", $rootLevel = TRUE, $parent="", $childName="")
{
  /**
   *  Only in the root of the xml we want to start the table
   *  @access private
   */
  if ($rootLevel === TRUE)
    {
      $return .= "<form method=\"post\" action=\"\" name=\"ext\">\n";
      $return .= "<input type=\"hidden\" name=\"ext\" value=\"exty\">\n";
      $return .= "<input type=\"submit\" value=\"save\">\n</form>\n";

      $return .= "<table border=\"1\">\n";
  }

  /**
   *  The prifix is use to have the indentation in the source code
   *  @access private
   */
  $prePrefFix = "\t";


  /**
   *   Check if there are childeren in the next level.
   *   If there are no more childeren then
   *   - if $handel is empty replace with a html whitespace
   *   - build a html table field with form input field
   *   - return to the previous level
   *   @access private
   */
  if ((count($handel->children() ) < 1 )  )
    {
      if ( empty($handel) ) $handel = "&nbsp;";
      $ret0 .=  $preffix . $ax . "<td><input type=\"text\" name=\"$childName\" value=\"" . $handel . "\" ></td>\n";
      return $ret0;
  }

  // Only wen there are no more childeren with the currend child we need to start
  // building a new row
  /**
   *  Only if the there are no childeren in the child we can start the
   *  row and the html form
   *  To know the name of the parent (categorie) we include the $parent
   *
   *   @access private
   */
  if (count($handel->children()->children() ) < 1 )
    {
      $return .= $preffix . "<tr>\n";
      $return .= $preffix . "  <form>\n";
      $return .= $preffix . $parent;
  }

  /**
   *  Check if the name of the tag we are going to use is the name tag
   *  if so we create a html table field with its value.
   *  This way we know ware we are in the xml file.
   *
   *  @access private
   */
  if ($handel->name)
    {
      $parent .= "\t<td>" . $handel->name . "</td>\n";
  }

  // go to the next level
  /**
   *   To know what is in side the childeren we need to rerun this function
   *
   *    @access private
   */
  foreach ($handel->children() as $key => $child)
    {
      $return .= RecursiveXMLTableEdit($child, $preffix .  $prePrefFix, FALSE, $parent, $child->getName());
  }

  /**
   *    We are don with this level.
   *    Lets place the submit button and close the html tags.
   *
   *    @access private
   */
  if (count($handel->children()->children() ) < 1 )
    {
      $return .= $preffix . "\t<td><input type=\"submit\" name=\"upd\" value=\"Submit\"></td>\n";
      $return .= $preffix . "  </form>\n";
      $return .= $preffix . "</tr>\n";
  }

  /**
   *    We are back in the rootlevel.
   *
   *    We are done with the xml al we now have to do is close the last html tag
   *
   *    @access private
   */
  if ($rootLevel === TRUE)
    {
      $return .= "</table>\n";
  }
  Return $return;
}
/**
 *  Create a html table of the xml, with input fields
 *
 *  If we need to be able to edit the content of the xml we can use this function
 *  It shows the content in a html table. All info in put in rows as a form
 *  so that it can be saved after the changes has been made.
 *
 *    @param simpleXMLobject  $handel     This can also be the xmlobject that is in a tag.
 *    @param string           $preffix    To make the sourcecode readable we need to add the prefix like \t
 *    @param boolean          $rootLevel  Optional, if we start the recurive we
 *                                        are not in thus need to be FALSE.
 *    @param string           $parent     Optional, While creating the table we
 *                                        need to know the name of the parent tag
 *                                        we are in. This a html table field string.
 *    @param string           $childName  Optional, while create the table we
 *                                        need to name of the tag we are printing.
 */
function recursiveXMLTableEditVer($handel, $preffix = "", $rootLevel = TRUE, $parent="", $childName="")
{
  /**
   *  Only in the root of the xml we want to start the table
   *  @access private
   */
  if ($rootLevel === TRUE)
    {
      $return .= "<form method=\"post\" action=\"\" name=\"ext\">\n";
      $return .= "<input type=\"hidden\" name=\"ext\" value=\"exty\">\n";
      $return .= "<input type=\"submit\" value=\"Save\">\n</form>\n";

      $return .= "<table border=\"1\">\n";
  }

  /**
   *  The prifix is use to have the indentation in the source code
   *  @access private
   */
  $prePrefFix = "\t";


  /**
   *   Check if there are childeren in the next level.
   *   If there are no more childeren then
   *   - if $handel is empty replace with a html whitespace
   *   - build a html table field with form input field
   *   - return to the previous level
   *   @access private
   */
  if ((count($handel->children() ) < 1 )  )
    {
      if ( empty($handel) ) $handel = "&nbsp;";
      $ret0 .=  $preffix . $ax . "<tr><td align=right>$childName :</td><td><input type=\"text\" name=\"$childName\" value=\"" . $handel . "\" ></td></tr>\n";
      return $ret0;
  }

  // Only wen there are no more childeren with the currend child we need to start
  // building a new row
  /**
   *  Only if the there are no childeren in the child we can start the
   *  row and the html form
   *  To know the name of the parent (categorie) we include the $parent
   *
   *   @access private
   */
  if (count($handel->children()->children() ) < 1 )
    {
//       $return .= $preffix . "<tr>\n";
      $return .= $preffix . "<tr><td><table>\n";
      $return .= $preffix . "  <form>\n";
//       $return .= $preffix . $parent;
  }

  /**
   *  Check if the name of the tag we are going to use is the name tag
   *  if so we create a html table field with its value.
   *  This way we know ware we are in the xml file.
   *
   *  @access private
   */
//   if ($handel->name)
//     {
//       $parent .= "\t<td>" . $handel->name . "</td>\n";
//   }

  // go to the next level
  /**
   *   To know what is in side the childeren we need to rerun this function
   *
   *    @access private
   */
  foreach ($handel->children() as $key => $child)
    {
      $return .= recursiveXMLTableEditVer($child, $preffix .  $prePrefFix, FALSE, $parent, $child->getName());
  }

  /**
   *    We are don with this level.
   *    Lets place the submit button and close the html tags.
   *
   *    @access private
   */
  if (count($handel->children()->children() ) < 1 )
    {
      $return .= $preffix . "\t<td><input type=\"submit\" name=\"upd\" value=\"Submit\"></td>\n";
      $return .= $preffix . "  </form>\n";
      $return .= $preffix . "</tr></table></td></tr>\n";
  }

  /**
   *    We are back in the rootlevel.
   *
   *    We are done with the xml al we now have to do is close the last html tag
   *
   *    @access private
   */
  if ($rootLevel === TRUE)
    {
      $return .= "</table>\n";
  }
  Return $return;
}

function RecursiveXML($handel, $scr = "xmlfile", $preffix = "") {
  // write out the xml object, this can be on screen or to a file
  // for $scr use: xmlfile, xmltable, text
  if (isset($handel) == TRUE)
    {
      if (!is_object($handel) )
          {
            $handel = simplexml_load_string($handel);
        }
//       else
//         {
//           echo "error";
//       }


      switch (strtolower($scr)) {
            Case "xmlfile":
                return RecursiveXMLFile($handel, $preffix);
                Break;
            Case "xmltable":
                return RecursiveXMLTable($handel, $preffix);
                Break;
            Case "text":
                return RecursiveXMLText($handel, $preffix);
                Break;
            Case "xmltableedit":
                return RecursiveXMLTableEdit($handel, $preffix);
            Case "xmltableeditv":
                return recursiveXMLTableEditVer($handel, $preffix);
      }
    }
  else
    {
      return FALSE;
  }
}
/* * ********************************************************************** * */
/**
 *    Function to check recursively if dirname is exists in directory's tree
 *
 *    @param string $dir_name
 *    @param string [$path]
 *    @return bool
 *    @author FanFataL
 */ /*
function dir_exists( $dir_name = FALSE, $path = "./" )
  {
    if ( $dir_name == FALSE ) return FALSE;

    if ( @is_dir( $path . $dir_name ) ) return TRUE;

//     $tree = glob( $path.'*', GLOB_ONLYDIR);
//     if($tree && count($tree)>0) {
//         foreach($tree as $dir)
//             if(dir_exists($dir_name, $dir.'/'))
//                 return TRUE;
//     }
//
//     return FALSE;
}*/

/**
 *
 *
 *    @param string     $file
 *    @param string     [$path]
 *    @param xmlobject  $xml
 *
 *    @return bool
 *
 *
 */
function fap_SaveFile( $file, $path = './', $xml )
{
  // get name of file, this is also the dit name used
  $len  = strlen( stristr( $file, "." ) );
  $name = substr( $file, 0, (-1*$len ) );

  // check if dir for backup exists if not create
  if (dir_exists( DIR_FS_SITEBAK . $name) == FALSE )
    {
      mkdir( ( DIR_FS_SITEBAK . $name ) , 0777, FALSE);
  }

  $newfile  = "";

  // check if the file we want to use exists, if not we do not need to backup
  if (file_exists($file) )
    {
      // preare for backup
      $newfile = DIR_FS_SITEBAK . $name . "/" . $file. "." . date("Ymd_Hi_s") .".bak";

      // create backup
      if ( copy( $file, $newfile ) == false ) echo "failed to copy $newfile...\n";
  }

  // the backup is done lets rewrite the file.
  $file_handle = fopen($file,"w") or die("Bestand niet schrijfbaar");
  fwrite($file_handle, $xml);
  fclose($file_handle);

  // cleanup.
  unset($_REQUEST["ext"]);
  unset($_SESSION["xml"]);
  unset($newfile);
  return TRUE;
}

/* * ********************************************************************** * */
function checkUrlGetWord()
{
  $ReadRequest = array();
//   $ReadRequest['categorie'] = $_REQUEST['categorie'];
//   $ReadRequest['en']        = $_REQUEST['en'];
//   $ReadRequest['il']        = $_REQUEST['il'];
//   $ReadRequest['ilm']       = $_REQUEST['ilm'];
//   $ReadRequest['ilf']       = $_REQUEST['ilf'];
//   $ReadRequest['ph']        = $_REQUEST['ph'];
//   $ReadRequest['phm']       = $_REQUEST['phm'];
//   $ReadRequest['phf']       = $_REQUEST['phf'];
  foreach ($_REQUEST as $key=>$value)
  {
//     if (is_array($value)) $ReadRequest[$key] =$value;
//     if (is_bool($value)) $ReadRequest[$key] =$value;
//     if (is_callable($value)) $ReadRequest[$key] =$value;
//     if (is_double($value)) $ReadRequest[$key] =$value;
//     if (is_float($value)) $ReadRequest[$key] =$value;
    if (is_int($value)) $ReadRequest[$key] =$value;
//     if (is_long($value)) $ReadRequest[$key] =$value;
    if (is_null($value)) $ReadRequest[$key] =$value;
//     if (is_numeric(trim($value)) $ReadRequest[$key] =trim($value);
//     if (is_object($value)) $ReadRequest[$key] =$value;
//     if (is_real($value)) $ReadRequest[$key] =$value;
//     if (is_resource($value)) $ReadRequest[$key] =$value;
//     if (is_scalar($value)) $ReadRequest[$key] =$value;
    if (is_string($value)) $ReadRequest[$key] = htmlentities($value, ENT_COMPAT, 'UTF-8');

    //Alias of is_int()
//     if (is_integer($value)) $ReadRequest[$key] =$value;
  }
}

// see sanitize.php !!
// function fop_checkValue($value)
// {
// //     if (is_array($value)) $ReadRequest[$key] =$value;
// //     if (is_bool($value)) $ReadRequest[$key] =$value;
// //     if (is_callable($value)) $ReadRequest[$key] =$value;
// //     if (is_double($value)) $ReadRequest[$key] =$value;
// //     if (is_float($value)) $ReadRequest[$key] =$value;
//     if (is_int($value)) return $value;
// //     if (is_long($value)) $ReadRequest[$key] =$value;
//     if (is_null($value)) return $value;
// //     if (is_numeric(trim($value)) return trim($value);
// //     if (is_object($value)) $ReadRequest[$key] =$value;
// //     if (is_real($value)) $ReadRequest[$key] =$value;
// //     if (is_resource($value)) $ReadRequest[$key] =$value;
// //     if (is_scalar($value)) $ReadRequest[$key] =$value;
//     if (is_string($value)) return htmlentities($value, ENT_COMPAT, 'UTF-8');
//
//     //Alias of is_int()
// //     if (is_integer($value)) $ReadRequest[$key] =$value;
//   Return FALSE;
// }

/**
 *  Save file and make backup
 *
 *  @param simpelXMLObject $sxe       the xml information
 *  @param string          $fileName  Filename used, we are going to exent de name
 *  @param string          $xmlRoot   this is the folder name in the backup dir
 *  @param string          $doctype   Need to be add in the xmlfile before saving
 */
function fop_SaveFile($sxe, $fileName, $xmlRoot, $docType= '') {

if (empty($sxe) == TRUE) {
echo "empty";
return FALSE;
}

  //Create the new filename with path.
  $newfile = DIR_FS_SITEBAK . $xmlRoot . "/" . $fileName. "." . date("Ymd_Hi_s") .".bak";

  //create a backup
  if (copy($fileName, $newfile) ) {
      //the backup is created now we can write

      //open the file, if not exists one will be created, while will br truncated
      //and the pointer is at the begin of the file.
      $file_handle = fopen($fileName,"w") or die("Bestand of map niet schrijfbaar");

      //create file content
      $content .="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n";
      $content .= $docType;
      $content .= $sxe;

      //write and close file
      fwrite($file_handle, $content);
      fclose($file_handle);
    }else{
      echo "failed to copy $newfile...\n";
  }
  unset($_REQUEST["ext"]);
//   unset($_SESSION["xml"]);
  unset($newfile);
}

?>
