<?php
class SoortObj {
    var $name;
    var $number;

    function SoortObj($number, $name)
    {
        $this->name   = $name;
        $this->number = $number;
    }

    /* This is the static comparing function: */
    static function cmp_obj($a, $b)
    {
        $al = strtolower($a->number);
        $bl = strtolower($b->number);
        if ($al == $bl) {
            return 0;
        }
        return ($al > $bl) ? +1 : -1;
    }
} // eof SoortObj

Function SoortXmlMisparim($pXml) {

    foreach ($pXml as $key=>$value) {
        $objXml[] = new SoortObj((intval($value->attributes() ) ), $value);
    }

    usort($objXml, array("SoortObj", "cmp_obj"));
    $xml = new simplexmlelement('<' . $pXml->getname() . '></' . $pXml->getname() . '>');

    foreach ($objXml as $key=>$parrent) {
        foreach ($parrent as $key=>$value) {
            // for the sort we give a number as value
            // while trying the extract information form the number as a
            // object a error is found and the scropt is stopt
            if (is_object($value) ) {
                $axml = $xml->addChild($value->getname());
                $axml->addAttribute($value->attributes()->getname(), $value->attributes() );

                foreach ($value as $key=>$lChild) {
                    $baxml = $axml->addChild($lChild->getname(), $lChild );
                    unset($baxml);
                } //eof foreach
            unset($axml);
            }// eof if
        }//eof foreach
    }//eof foreach

//     echo RecursiveXML($xml, xmltable);
    Return RecursiveXML($xml);
}
/********************************************************************************************************************/

/********************************************************************************************************************/
?>