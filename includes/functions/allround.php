<?php

  Function FColor($pReal, $pUser) {
    if ($pReal != $pUser) {
      $pValue = "bgcolor=\"red\"";
      Return $pValue;
    }
  }

  Function FScoreBG($pScore) {
    $Return;
    switch ($pScore) {
      case 0:
        $Return = "bgcolor=\"red\"";
        break;
      case ($pScore > 0 && $pScore < 60 ):
        $Return = "bgcolor=\"red\"";
        break;
      case ($pScore < 90 && $pScore >= 60):
        $Return = "bgcolor=\"orange\"";
        break;
      case ($pScore >= 90):
        $Return = "bgcolor=\"#00FF00\"";
        break;
      }
    Return $Return;
  }

  // todo waar zijn de parameter? de functie is nu alleen hier te gebruiken
  // @param array?
  // @reutn mixed numberWorng or false?
  Function FFault() {
    // Count how many there are wrong. Misparim
    $fout = "0";
    foreach ($_GET as $key => $data) {
        if (is_numeric($key) ) {
            if (!empty($data) ) {
                if ($key!=$data) {
                    $fout++;
                }
            }
        }
    }
    Return($fout);
  }

  Function FNotFilled() {
    // Count how many there are wrong.
    $NotFilled = "0";
    foreach ($_GET as $key => $data) {
        if (is_numeric($key) ) {
            if ($data=="") {
                $NotFilled++;
            }
        }
    }
    Return($NotFilled);
  }

Function FNumberTest($Number) {
  if (is_numeric($Number) ) {
    Return ($Number);
  }
}

/* * ********************************************************************************* * */

class SoortObj {
    var $name;
    var $number;

    function SoortObj($number, $name)
    {
        $this->name   = $name;
        $this->number = $number;
    }

    /* This is the static comparing function: */
    static function cmp_obj($a, $b)
    {
        $al = strtolower($a->number);
        $bl = strtolower($b->number);
        if ($al == $bl) {
            return 0;
        }
        return ($al > $bl) ? +1 : -1;
    }
} // eof SoortObj

Function SoortXML($pXml) {

    foreach ($pXml as $key=>$value) {
        $objXml[] = new SoortObj((intval($value->attributes() ) ), $value);
    }

    usort($objXml, array("SoortObj", "cmp_obj"));
    $xml = new simplexmlelement('<' . $pXml->getname() . '></' . $pXml->getname() . '>');

    foreach ($objXml as $key=>$parrent) {
        foreach ($parrent as $key=>$value) {
            // for the sort we give a number as value
            // while trying the extract information form the number as a
            // object a error is found and the scropt is stopt
            if (is_object($value) ) {
                $axml = $xml->addChild($value->getname());
                $axml->addAttribute($value->attributes()->getname(), $value->attributes() );

                foreach ($value as $key=>$lChild) {
                    $baxml = $axml->addChild($lChild->getname(), $lChild );
                    unset($baxml);
                } //eof foreach
            unset($axml);
            }// eof if
        }//eof foreach
    }//eof foreach

//     echo RecursiveXML($xml, xmltable);
    Return RecursiveXML($xml);
}
/* * ********************************************************************************* * */

/*
 *    Function to check recursively if dirname is exists in directory's tree
 *
 *    Found on http://nl.php.net/manual/en/function.is-dir.php
 *
 *    @param  string $dir_name
 *    @param  string [$path]
 *    @return bool
 *    @author FanFataL
 *
 *    @todo   Remember that the owner of the directory has to be the same of the
 *            script user, otherwise this function will always return false when
 *            PHP is running in safe_mode..
 *
 *            Actually, is_dir is not affected by UID checks in safe_mode.
 *            is_dir will even return true if you lack sufficient privileges to
 *            read said directory.
 */
function dir_exists($dir_name = false, $path = './') {
    if(!$dir_name) return false;

    if(is_dir($path.$dir_name)) return true;

    $tree = glob($path.'*', GLOB_ONLYDIR);
    if($tree && count($tree)>0) {
        foreach($tree as $dir)
            if(dir_exists($dir_name, $dir.'/'))
                return true;
    }

    return false;
}
?>
