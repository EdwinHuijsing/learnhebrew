<?php

/**
 * associative array to xml transformation class
 *
 * @PHPVER    5.0
 *
 * @author    Johnny Brochard
 * @ver        0001.0002
 * @date    25/08/04
 */


class CArray2xml2array {

    /*
     * XML Array
     * @var array
     * @access private
     */
    private $XMLArray;

    /*
     * array is OK
     * @var bool
     * @access private
     */
    private $arrayOK;

    /*
     * XML file name
     * @var string
     * @access private
     */
    private $XMLFile;

    /*
     * file is present
     * @var bool
     * @access private
     */
    private $fileOK;

    /*
     * DOM document instance
     * @var DomDocument
     * @access private
     */
    private $doc;

    /**
     * Constructor
     * @access public
     */

    public function CArray2xml2array(){

    }

    /**
     * setteur setXMLFile
     * @access public
     * @param string $XMLFile
     * @return bool
     */
    public function setXMLFile($XMLFile){
        if (file_exists($XMLFile)){
            $this->XMLFile = $XMLFile;
            $this->fileOK = true;
        }else{
            $this->fileOK = false;
        }
        return $this->fileOK;
    }

    /**
     * saveArray
     * @access public
     * @param string $XMLFile
     * @return bool
     */
    public function saveArray($XMLFile, $rootName="", $encoding="utf-8")
    {
        global $debug;
        $this->doc = new domdocument("1.0", $encoding);
        $arr = array();
        if (count($this->XMLArray) > 1){
            if ($rootName != ""){
                $root = $this->doc->createElement($rootName);
            }else{
                $root = $this->doc->createElement("root");
                $rootName = "root";
            }
            $arr = $this->XMLArray;
        }else{

            $key = key($this->XMLArray);
            $val = $this->XMLArray[$key];

            if (!is_int($key)){
                $root = $this->doc->createElement($key);
                $rootName = $key;
            }else{
                if ($rootName != ""){
                    $root = $this->doc->createElement($rootName);
                }else{
                    $root = $this->doc->createElement("root");
                    $rootName = "root";
                }
            }
            $arr = $this->XMLArray[$key];
        }

        $root = $this->doc->appendchild($root);

        $this->addArray($arr, $root, $rootName);

/*        foreach ($arr as $key => $val){
            $n = $this->doc->createElement($key);
            $nText = $this->doc->createTextNode($val);
            $n->appendChild($nodeText);
            $root->appendChild($n);
        }
*/

//         if ($this->doc->save($XMLFile) == 0){
//             return false;
//         }else{
//             return true;
//         }
    }

    /**
     * addArray recursive function
     * @access public
     * @param array $arr
     * @param DomNode &$n
     * @param string $name
     */
    function addArray($arr, &$n, $name=""){
        foreach ($arr as $key => $val){
            if (is_int($key)){
                if (strlen($name)>1){
                    $newKey = substr($name, 0, strlen($name)-1);
                }else{
                    $newKey="item";
                }
            }else{
                $newKey = $key;
            }
echo $newKey;
            $node = $this->doc->createElement($newKey);
//             if (is_array($val)){
//                 $this->addArray($arr[$key], $node, $key);
//             }else{
//                 $nodeText = $this->doc->createTextNode($val);
//                 $node->appendChild($nodeText);
//             }
//             $n->appendChild($node);
        }
    }


    /**
     * setteur setArray
     * @access public
     * @param array $XMLArray
     * @return bool
     */
    public function setArray($XMLArray){
        if (is_array($XMLArray) && count($XMLArray) != 0){
            $this->XMLArray = $XMLArray;
            $this->arrayOK = true;
        }else{
            $this->arrayOK = false;
        }

        return $this->arrayOK;
    }

} // eof classCArray2xml2array





/**
 *
 * Array 2 XML class
 * Convert an array or multi-dimentional array to XML
 *
 * @author Kevin Waterson
 * @copyright 2009 PHPRO.ORG
 *
 */
class array2xml extends DomDocument
{

    public $nodeName;

    private $xpath;

    private $root;

    private $node_name;


    /**
    * Constructor, duh
    *
    * Set up the DOM environment
    *
    * @param    string    $root        The name of the root node
    * @param    string    $nod_name    The name numeric keys are called
    *
    */
    public function __construct($root='root', $node_name='node')
    {
        parent::__construct();

        /*** set the encoding ***/
        //$this->encoding = "ISO-8859-1";
        $this->encoding = "utf-8";

        /*** format the output ***/
        $this->formatOutput = true;

        /*** set the node names ***/
        $this->node_name = $node_name;

        /*** create the root element ***/
        $this->root = $this->appendChild($this->createElement( $root ));

        $this->xpath = new DomXPath($this);
    }

    /*
    * creates the XML representation of the array
    *
    * @access    public
    * @param    array    $arr    The array to convert
    * @aparam    string    $node    The name given to child nodes when recursing
    *
    */
    public function createNode( $arr, $node = null)
    {
        if (is_null($node))
        {
            $node = $this->root;
        }
        foreach($arr as $element => $value)
        {
            $element = is_numeric( $element ) ? $this->node_name : $element;
            $value   = is_array( $value ) ? strvar("a") : $value;
//             $child = $this->createElement($element, (is_array($value) ? null : $value) );
// $a = is_array ( $value ) ? " " : $value );
            $child = $this->createElement( $element, $value);
//             $node->appendChild($child);

//             if (is_array($value))
//             {
//                 self::createNode($value, $child);
//             }
        }
    }

    /*
    * Return the generated XML as a string
    *
    * @access    public
    * @return    string
    *
    */
    public function __toString()
    {
        return $this->saveXML();
    }

    /*
    * array2xml::query() - perform an XPath query on the XML representation of the array
    * @param str $query - query to perform
    * @return mixed
    */
    public function query($query)
    {
        return $this->xpath->evaluate($query);
    }

} // end of class


class ArrayToXML
{
    /**
     * The main function for converting to an XML document.
     * Pass in a multi dimensional array and this recrusively loops through and builds up an XML document.
     *
     * @param array $data
     * @param string $rootNodeName - what you want the root node to be - defaultsto data.
     * @param SimpleXMLElement $xml - should only be used recursively
     * @return string XML
     */
    public static function toXML( $data, $rootNodeName = 'ResultSet', &$xml=null ) {

        // turn off compatibility mode as simple xml throws a wobbly if you don't.
        if ( ini_get('zend.ze1_compatibility_mode') == 1 ) ini_set ( 'zend.ze1_compatibility_mode', 0 );
        if ( is_null( $xml ) )
          {
          $content .="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n";
          $content .="<" . $rootNodeName . "></" . $rootNodeName . ">";
          $xml = simplexml_load_string($content);
        }

        // loop through the data passed in.
         foreach( $data as $key => $value ) {

            // no numeric keys in our xml please!
             if ( is_numeric( $key ) ) {
                $numeric = 1;
                $key = $rootNodeName;
            }


            // delete any char not allowed in XML element names
//             $key = preg_replace('/[^a-z0-9\-\_\.\:]/i', '', $key);


            // if there is another array found recrusively call this function
            if ( is_array( $value ) ) {
               $node = ArrayToXML::is_Assoc( $value ) || ($numeric ? $xml->addChild( $key ) : $xml);

                // recrusive call.
                if ( $numeric ) $key = 'anon';
echo a;
                 ArrayToXML::toXml($value, $key, $node);
echo b;
            } else {
echo c;
                // add single node.
                $value = htmlentities( $value );
echo d;echo $value;
                $xml->addChild( $key, $value );
echo e;
            }
         }

        // pass back as XML
         return $xml->asXML();

    // if you want the XML to be formatted, use the below instead to return the XML
        //$doc = new DOMDocument('1.0');
        //$doc->preserveWhiteSpace = false;
        //$doc->loadXML( $xml->asXML() );
        //$doc->formatOutput = true;
        //return $doc->saveXML();*/
    }


    /**
     * Convert an XML document to a multi dimensional array
     * Pass in an XML document (or SimpleXMLElement object) and this recrusively loops through and builds a representative array
     *
     * @param string $xml - XML document - can optionally be a SimpleXMLElement object
     * @return array ARRAY
     */
    public static function toArray( $xml ) {
        if ( is_string( $xml ) ) $xml = new SimpleXMLElement( $xml );
        $children = $xml->children();
        if ( !$children ) return (string) $xml;
        $arr = array();
        foreach ( $children as $key => $node ) {
            $node = ArrayToXML::toArray( $node );

            // support for 'anon' non-associative arrays
            if ( $key == 'anon' ) $key = count( $arr );

            // if the node is already set, put it into an array
            if ( isset( $arr[$key] ) ) {
                if ( !is_array( $arr[$key] ) || $arr[$key][0] == null ) $arr[$key] = array( $arr[$key] );
                $arr[$key][] = $node;
            } else {
                $arr[$key] = $node;
            }
        }
        return $arr;
    }

    // determine if a variable is an associative array
    private static function is_Assoc( $array ) {
        return (is_array($array) && 0 !== count(array_diff_key($array, array_keys(array_keys($array)))));
    }
}

?>
