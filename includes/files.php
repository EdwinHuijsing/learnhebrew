<?php
// column and box
Define("FILE_LEFT", "left.php");
Define("FILE_RIGHT", "right.php");
Define("FILE_HEAD", "head.php");
Define("FILE_FOOT", "foot.php");
// Define("", "");

//xml files
Define("FILE_XML_MISPARIM", "misparim.xml");
Define("FILE_XML_ALEFBET", "AlefBet.xml");
Define("FILE_XML_MILLIM", "millim.xml");

// functions
Define("FILE_SANITIZE", "sanitize.php");
?>
