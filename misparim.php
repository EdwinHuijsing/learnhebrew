<?php
Require_once ("includes/configure.php");
include_once (DIR_WS_INCLUDES . "head.php");
?>
<table class=main width="800" border=1>
  <tr class=main>
<!-- bof left column-->
    <td width="100" valign=top>
      <?php include_once(DIR_WS_INCLUDES . FILE_LEFT); ?>
    </td>
<!-- eof left column -->
<!-- bof main column -->
    <td width="600" align=center>
      <?php
      if (!isset($_REQUEST['type']) )
        {
          include_once('misparim/overview.php');
        }
      else
        {
          switch ($_REQUEST['type'])
            {
              case 'hebr':
                  include_once('misparim/misparimHebr.php');
                  break;
              case 'phon':
                  include_once('misparim/misparimPhon.php');
                  break;
              case 'mitp':
                  include_once('misparim/misparimCheck.php');
                  break;
              case 'mith':
                  include_once('misparim/misparimCheck.php');
                  break;
              default :
                  echo "Sorry there is a error";
                  break;
          }
      }
      ?>
    </td>
<!-- eof main column-->
<!--bof right column -->
    <td width="100" valign=top>
    <?php include_once(DIR_WS_INCLUDES . FILE_RIGHT); ?>
    </td>
<!-- eof right column -->
  </tr>
</table>
<?php include_once(DIR_WS_INCLUDES . FILE_FOOT); ?>
