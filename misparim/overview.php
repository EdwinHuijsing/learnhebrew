<?php
  // Check if file exists
  if (file_exists(DIR_WS_XML . FILE_XML_MISPARIM)) {
    echo "\t\t\t<table class=quiz border=1><!-- bof overview -->\n";

    //Load file in to a object
    $objRead = simplexml_load_file(DIR_WS_XML . FILE_XML_MISPARIM);
    $Counter=0;

    //Loop trough the root en find the the first level (AlefBet)
    foreach ($objRead as $key => $Value) {
        $number = $objRead->mispar[$Counter]->Number;
        $il     = $objRead->mispar[$Counter]->il;
        $ilm    = $objRead->mispar[$Counter]->ilm;
        $ilf    = $objRead->mispar[$Counter]->ilf;
        $ilPh   = $objRead->mispar[$Counter]->ilPh;
        $ilPhM  = $objRead->mispar[$Counter]->ilPhM;
        $ilPhF  = $objRead->mispar[$Counter]->ilPhF;

        if ($il    =="") $il    = "&nbsp;";
        if ($ilm   =="") $ilm   = "&nbsp;";
        if ($ilf   =="") $ilf   = "&nbsp;";
        if ($ilPh  =="") $ilPh  = "&nbsp;";
        if ($ilPhM =="") $ilPhM = "&nbsp;";
        if ($ilPhF =="") $ilPhF = "&nbsp;";

        echo "\t\t\t\t<tr class=overview>\n";
        echo "\t\t\t\t\t<td align=right >" .$number. "</td>\n";
        echo "\t\t\t\t\t<td             >" .$il. "</td>\n";
        echo "\t\t\t\t\t<td             >" .$ilm. "</td>\n";
        echo "\t\t\t\t\t<td             >" .$ilf. "</td>\n";
        echo "\t\t\t\t\t<td             >" .$ilPh. "</td>\n";
        echo "\t\t\t\t\t<td             >" .$ilPhM. "</td>\n";
        echo "\t\t\t\t\t<td             >" .$ilPhF. "</td>\n";
        echo "\t\t\t\t</tr>\n";
        $Counter++;
      }
      echo "\t\t\t</TABLE><!-- eof overview -->\n";
    }else{
    echo "Bestand bestaat niet";
  }
?>
