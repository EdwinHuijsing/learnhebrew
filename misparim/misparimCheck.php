<?php
  Include_once (DIR_WS_FUNCTIONS . "allround.php");
  $objXml = simplexml_load_file(DIR_WS_XML . FILE_XML_MISPARIM);
?>
<table class=quiz>
  <tr>
    <td valign=top> <!-- ******************** BOF score ******************** -->
      <?php
          $QtyLetters = 0;
          foreach ($_GET as $key => $data) {
              if (is_numeric($key) ) {
                  $QtyLetters++;
              }
          }
          $Fault      = FFault();
          $NotFilled  = FNotFilled();
          $Good       = $QtyLetters-$Fault-$NotFilled;
          $Score      = intval(round((($Good/$QtyLetters)*100),1) );
      ?>
      <table class=left>
        <tr>
          <td align=center colspan=2><b>from <?php echo $QtyLetters; ?> are</b></td>
        </tr>
        <tr>
          <td class=ScoreLeft>Wrong:</td>
          <td><?php echo $Fault; ?></td>
        </tr>
        <tr>
          <td class=ScoreLeft>Not filled:</td>
          <td><?php echo $NotFilled; ?></td>
        </tr>
        <tr>
          <td class=ScoreLeft>Good:</td>
          <td><?php echo $Good; ?></td>
        </tr>
        <tr>
          <td class=ScoreLeft>Score:</td>
          <td <?php echo FScoreBG($Score);?>><?php echo $Score; ?>%</td>
        </tr>
      </table>
    </td> <!-- ******************** EOF score ******************** -->
    <td>
      <table border=1>
        <tr>
          <td>Onzijdig</td>
          <td>Male</td>
          <td>Female</td>
          <td>Place</td>
          <td>You</td>
          <td>Onzijdig</td>
          <td>Male</td>
          <td>Female</td>
          <td>Place</td>
          <td>You</td>
        </tr>
      <?php
        $Teller=1;
        if ( $_REQUEST['type']=="mith") {
            $type = "il";
            $typeM = "ilm";
            $typeF = "ilf";
            $classs = "letter";
          }else{
            $type = "ilPh";
            $typeM = "ilPhM";
            $typeF = "ilPhF";
            $classs = "Main";
        }

        foreach ($_GET as $key => $data){
            if (is_numeric($key)) {
                $sxe = $objXml->xpath('//mispar[@id="' . $key . '"]');
                if (($Teller%2)>0) {
                    echo "\t\t\t<tr>\n";
                    echo "\t\t\t\t<td class=" . $classs . ">". $sxe[0]->$type . "&nbsp;</td>\n";
                    echo "\t\t\t\t<td class=" . $classs . ">". $sxe[0]->$typeM . "&nbsp;</td>\n";
                    echo "\t\t\t\t<td class=" . $classs . ">". $sxe[0]->$typeF . "&nbsp;</td>\n";
                    echo "\t\t\t\t<td class=Main>" . $key . "&nbsp;</td>\n";
                    echo "\t\t\t\t<td class=\"Main\" " . FColor( $sxe[0]->Attributes(), $data) . ">" .  $data . "&nbsp;</td>\n";
                  }else{
                    echo "\t\t\t\t<td class=" . $classs . ">". $sxe[0]->$type . "&nbsp;</td>\n";
                    echo "\t\t\t\t<td class=" . $classs . ">". $sxe[0]->$typeM . "&nbsp;</td>\n";
                    echo "\t\t\t\t<td class=" . $classs . ">". $sxe[0]->$typeF . "&nbsp;</td>\n";
                    echo "\t\t\t\t<td class=Main>" . $key . "&nbsp;</td>\n";
                    echo "\t\t\t\t<td class=Main " . FColor( $sxe[0]->Attributes(), $data) . ">" . $data . "&nbsp;</td>\n";
                    echo "\t\t\t</tr>\n";
                }

                $Teller++;
            }
        }
        unset($type);
        unset($classs);
        if (($Teller%2)==0) {
            echo "\t\t\t<td colspan=5>&nbsp;</td>\n";
        }
      ?>
      </tr>
      </table>
    </td>
  </tr>
</table>
</BODY>
</HTML>
