<?php
/*  include("Number.php");
Function FNumberTest($Number) {
  if (is_numeric($Number) ) {
    Return ($Number);
  }
}

$Start = FNumberTest($_GET["start"]);
$End   = FNumberTest($_GET["end"]);
*/
/* ******************** BOF Functios ******************** */
/*  Function FColor($pReal, $pUser) {
    if ($pReal !== $pUser) {
      echo " bgcolor=\"red\"";
    }
  }

  Function FScoreBG($pValue) {
    switch ($pValue) {
      case 0:
        echo " bgcolor=\"red\"";
        break;
      case ($pValue > 0 && $pValue < 60 ):
        echo " bgcolor=\"red\"";
        break;
      case ($pValue < 90 && $pValue >= 60):
        echo " bgcolor=\"orange\"";
        break;
      case ($pValue >= 90):
        echo " bgcolor=\"#00FF00\"";
        break;
    }
  }

  Function FFault() {
    // Count how many there are wrong.
    $fout = "0";
    foreach ($_GET as $key => $data) {
      if (is_numeric($key) ) {
        if (!empty($data) ) {
          if ($key!=$data) {
            $fout++;
          }
        }
      }
    }
    Return($fout);
  }

  Function FNotFilled() {
    // Count how many there are wrong.
    $NotFilled = "0";
    foreach ($_GET as $key => $data) {
      if ($data == "" ) {
        $NotFilled++;
      }
    }
    Return($NotFilled);
  }
*/
/* ******************** EOF Functios ******************** */
/*
<HTML>
<header>
  <link rel="stylesheet" type="text/css" href="stylesheet.css">
</header>
<BODY>*/
/*<center>
<table>
  <tr>
    <td valign=top> <!-- ******************** BOF score ******************** -->
      <?php
        $QtyNumbers = 0;
        foreach ($_GET as $key => $data) {
          if (is_numeric($key) ) {
            $QtyNumbers++;
          }
        }
        $Fault      = FFault();
        $NotFilled  = FNotFilled();
        $Good       = $QtyNumbers-$Fault-$NotFilled;
        $Score      = intval(round((($Good/$QtyNumbers)*100),1) );
      ?>
      <table class=left>
        <tr>
          <td align=center colspan=2><b>from <?php echo $QtyNumbers; ?> are</b></td>
        </tr>
        <tr>
          <td class=ScoreLeft>Wrong:</td>
          <td><?php echo $Fault; ?></td>
        </tr>
        <tr>
          <td class=ScoreLeft>Not filled:</td>
          <td><?php echo $NotFilled; ?></td>
        </tr>
        <tr>
          <td class=ScoreLeft>Good:</td>
          <td><?php echo $Good; ?></td>
        </tr>
        <tr>
          <td class=ScoreLeft>Score:</td>
          <td<?php FScoreBG($Score); ?>><?php echo $Score; ?>%</td>
        </tr>
      </table>
      <a href="NumberOrder.php?start=<?php echo $Start;?>&amp;end=<?php echo $End;?>">Try again!!</a>
    </td> <!-- ******************** EOF score ******************** -->
    <td>
      <table border=1>
        <tr>
          <td>Number</td>
          <td>Place</td>
          <td>You</td>
          <td>Number</td>
          <td>Place</td>
          <td>You</td>
        </tr>
      <?php
        $Teller=1;
        foreach ($_GET as $key => $data){
          if (is_numeric($key) ) {
            if (empty($arrNumber[$key]["ilPh"]) ) {
              $Number = "ilPhF";
            } else {
              $Number = "ilPh";
            }
            if (($Teller%2)>0) {
      ?>
        <tr>
          <td><?php echo $arrNumber[$key][$Number];?></td>
          <td Class=Main><?php
            echo $key;
          ?></td>
          <td Class=Main<?php FColor($arrNumber[$key]["Number"], $data);?>><?php echo $data;?>&nbsp;</td>
        <?php }else{ ?>
          <td><?php echo $arrNumber[$key][$Number];?></td>
          <td Class=Main><?php
              echo $key;
          ?></td>
          <td Class=Main<?php FColor($arrNumber[$key]["Number"], $data);?>><?php echo $data;?>&nbsp;</td>
        </tr>
        <?php }
              $Teller++;
            }
          }
        if (($Teller%2)==0) {
        ?>
          <td colspan=3>&nbsp;</td>
        </tr>
<?php   }?>
      </table>
    </td>
  </tr>
</table>
</center>
</BODY>
</HTML>
*/
  Include (DIR_WS_FUNCTIONS . "allround.php");
  $objAlefBet = simplexml_load_file(DIR_WS_XML . FILE_XML_MISPARIM);

  echo "<table class=quiz>\n";
  echo "\t<tr>\n";
  echo "\t\t<td valign=top> <!-- ******************** BOF score ******************** -->\n";
    $QtyLetters = 0;
    foreach ($_GET as $key => $data) {
        if (is_numeric($key) ) {
            $QtyLetters++;
        }
    }
    $Fault      = FFault();
    $NotFilled  = FNotFilled();
    $Good       = $QtyLetters-$Fault-$NotFilled;
    $Score      = intval(round((($Good/$QtyLetters)*100),1) );
  echo "\t\t<table class=left>\n";
  echo "\t\t\t<tr>\n";
  echo "\t\t\t\t<td align=center colspan=2><b>from " . $QtyLetters . " are</b></td>\n";
  echo "\t\t\t</tr>\n";
  echo "\t\t\t<tr>\n";
  echo "\t\t\t\t<td class=ScoreLeft>Wrong:</td>\n";
  echo "\t\t\t\t<td>" . $Fault . "</td>\n";
  echo "\t\t\t</tr>\n";
  echo "\t\t\t<tr>\n";
  echo "\t\t\t\t<td class=ScoreLeft>Not filled:</td>\n";
  echo "\t\t\t\t<td>" . $NotFilled . "</td>\n";
  echo "\t\t\t</tr>\n";
  echo "\t\t\t<tr>\n";
  echo "\t\t\t\t<td class=ScoreLeft>Good:</td>\n";
  echo "\t\t\t\t<td>" . $Good . "</td>\n";
  echo "\t\t\t</tr>\n";
  echo "\t\t\t<tr>\n";
  echo "\t\t\t\t<td class=ScoreLeft>Score:</td>\n";
  echo "\t\t\t\t<td " . FScoreBG($Score) . ">" . $Score . "%</td>\n";
  echo "\t\t\t</tr>\n";
  echo "\t\t</table>\n";
  echo "\t\t</td> <!-- ******************** EOF score ******************** -->\n";
  echo "\t\t<td>\n";
  echo "\t\t\t<table border=1>\n";
  echo "\t\t\t<tr>\n";
  echo "\t\t\t\t<td>Letter</td>\n";
  echo "\t\t\t\t<td>Place</td>\n";
  echo "\t\t\t\t<td>You</td>\n";
  echo "\t\t\t\t<td>Letter</td>\n";
  echo "\t\t\t\t<td>Place</td>\n";
  echo "\t\t\t\t<td>You</td>\n";
  echo "\t\t\t</tr>\n";
  $Teller=1;
  if ( $_REQUEST['type']=="mith") {
      $type = "unicode";
      $classs = "letter";
    }else{
      $type = "name";
      $classs = "Main";
  }

  foreach ($_GET as $key => $data){

      if (is_numeric($key)) {
          if (($Teller%2)>0) {
              echo "\t\t\t<tr>\n";
              echo "\t\t\t\t<td class=" . $classs . ">". $objAlefBet->AlefBet[0]->letter[($key-1)]->$type . "</td>\n";
              echo "\t\t\t\t<td class=Main>" . $key . "</td>\n";
              echo "\t\t\t\t<td class=\"Main\" " . FColor( $objAlefBet->AlefBet[0]->letter[($key-1)]->Attributes(), $data) . ">" .  $data . "&nbsp;</td>\n";
            }else{
              echo "\t\t\t\t<td class=" . $classs . ">" . $objAlefBet->AlefBet[0]->letter[$key-1]->$type . "</td>\n";
              echo "\t\t\t\t<td class=Main>" . $key . "</td>\n";
              echo "\t\t\t\t<td class=Main " . FColor( $objAlefBet->AlefBet[0]->letter[($key-1)]->Attributes(), $data) . ">" . $data . "&nbsp;</td>\n";
              echo "\t\t\t</tr>\n";
          }
          $Teller++;
      }
  }
  unset($type);
  unset($classs);
  echo "\t\t\t<td colspan=3>&nbsp;</td>\n";
  echo "\t\t\t</tr>\n";
  echo "\t\t\t</table>\n";
  echo "\t\t</td>\n";
  echo "\t</tr>\n";
  echo "</table>\n";
  echo "</BODY>\n";
  echo "</HTML>\n";
?>
