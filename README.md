# Learn Hebrew
This project has been created to help some with learning Hebrew basics.  
The project started in 2008. Array's have been chosen as back-end because they are easy to handle.  
In 2008-4 the web-server had still PHP3.  

*Update 1*:  
Due to an other project, the back-end has moved from Array's to XML and from PHP3 too PHP4. This update started in 2009-7, and ended somewhere in 2010. Now that the back-end is XML there also is a need for a editting functions (admin), also the layout has got an update.  
Still this code is learning code, that's why there not all files are written the way it should be.  
Note: Not all files are needed, this project needs clean-up.

*Update 2*:  
Due to some events (can't remember what) i had moved the admin files in the admin directory. Because of this i had to update the code (2016).

# Important
The admin file is a security risk if it is online, because files are written to disk. No login is pressent.
