<?php
  // Check if file exists
  if (file_exists(DIR_WS_XML . FILE_XML_ALEFBET)) {
    echo "\t\t\t<table class=quiz border=1><!-- bof overview -->\n";

    //Load file in to a object
    $objRead = simplexml_load_file(DIR_WS_XML . FILE_XML_ALEFBET);
    $Counter=0;

    //Loop trough the root en find the the first level (AlefBet)
    foreach ($objRead as $key => $Value) {

      // check the attribute "from" (<AlefBet Form="Sofit">)
      if ($objRead->AlefBet[$Counter]->attributes() == "Basic" ) {

        $CountLetter = 0;

        //count how many letters we have in this AlefBet
        $QtyLetters  = count($objRead->AlefBet[$Counter]);
        foreach ($objRead->AlefBet[$Counter] as $kkey => $vvalue) {

          //in the table we have 2 kolumns we need to kwone where we are
          $even = ($CountLetter+1)%2;
          if ($even>0) {
              echo "\t\t\t\t<tr>\n";
              echo "\t\t\t\t\t<td             >" .$objRead->AlefBet[$Counter]->letter[$CountLetter]->place. "</td>\n";
              echo "\t\t\t\t\t<td class=letter>" .$objRead->AlefBet[$Counter]->letter[$CountLetter]->char. "</td>\n";
              echo "\t\t\t\t\t<td             >" .$objRead->AlefBet[$Counter]->letter[$CountLetter]->name. "</td>\n";
            }else{
              echo "\t\t\t\t\t<td             >&nbsp;</td>\n";
              echo "\t\t\t\t\t<td             >" .$objRead->AlefBet[$Counter]->letter[$CountLetter]->place. "</td>\n";
              echo "\t\t\t\t\t<td class=letter>" .$objRead->AlefBet[$Counter]->letter[$CountLetter]->char. "</td>\n";
              echo "\t\t\t\t\t<td             >" .$objRead->AlefBet[$Counter]->letter[$CountLetter]->name. "</td>\n";
              echo "\t\t\t\t</tr>\n";
            }

            //check if the table ends correct
            if ((($QtyLetters-1)==$CountLetter) && ($even>0) ) {
              echo "\t\t\t\t\t<td colspan='4'>&nbsp;</td>\n";
              echo "\t\t\t\t</tr>\n";
              }
          $CountLetter++;
          }
        }
        $Counter++;
      }
      echo "\t\t\t</TABLE><!-- eof overview -->\n";
    }else{
    echo "Bestand bestaat niet";
  }
?>
