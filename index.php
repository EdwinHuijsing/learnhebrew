<?php
// phpinfo();
require_once("includes/configure.php");
include_once(DIR_WS_INCLUDES . "head.php");
?>
<center>
<table class=main width="800" border=1>
  <tr valign=top>
<!--  bof left column -->
    <td width="100">
      <?php include_once(DIR_WS_INCLUDES . FILE_LEFT); ?>
    </td>
<!-- eof left column -->
<!-- bof main column -->
    <td width="600">
      <b>AlefBet</b><br>
      - <a href="alefbet.php">Overview</a><br>
      - <a href="alefbet.php?type=phon" target="_blank">Phonetic</a><br>
      - <a href="alefbet.php?type=hebr" target="_blank">Hebrew</a><br>
      <br>
      <b>Numbers (Mispar)</b><br>
      - <a href="misparim.php" target="_blank">Overview</a><br>
      - See side menu for more options.<br>
      <br>
      <b>Words</b><br>
      - <a href="millim.php"           target="_blank">Overview</a><br>
      - <a href="millim.php?type=phon" target="_blank">Phonetic</a><br>
      - <a href="millim.php?type=hebr" target="_blank">Hebrew</a><br>
    </td>
<!-- bof main column -->
<!-- bof right column-->
    <td width="100">
      <?php include_once(DIR_WS_INCLUDES . FILE_RIGHT); ?>
    </td>
<!-- bof right column-->
  </tr>
</table>
</center>
<?php include_once(DIR_WS_INCLUDES . FILE_FOOT);?>
