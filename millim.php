<?php
require_once ("includes/configure.php");
include_once (DIR_WS_INCLUDES . "head.php");
?>

<table class=main width="800" border=1>
  <tr class=main>
    <td width="100" valign=top>
<!-- bof left-->
      <?php include_once(DIR_WS_INCLUDES . FILE_LEFT); ?>
      &nbsp;
    </td>
<!-- eof left-->
<!-- bof main-->
    <td width="600" align=center>
        <?php
        if (!isset($_REQUEST['type']) )
          {
            include_once('millim/millimView.php');
          }
        else
          {
            switch ($_REQUEST['type'])
              {
                case 'hebr':
                    include_once('millim/millimHebr.php');
                    break;
                case 'phon':
                    include_once('millim/millimPhon.php');
                    break;
                case 'milchp':
                    include_once('millim/millimCheck.php');
                    break;
                case 'milchh':
                    include_once('millim/millimCheck.php');
                    break;
                default :
                    echo "Sorry there is a error";
                    break;
            }
        }
        ?>
    </td>
<!-- eof main-->
<!--bof right-->
    <td width="100" valign=top>
      <?php include_once(DIR_WS_INCLUDES . FILE_RIGHT); ?>
    </td>
<!-- eof right -->
  </tr>
</table>

<?php include_once(DIR_WS_INCLUDES . FILE_FOOT); ?>
